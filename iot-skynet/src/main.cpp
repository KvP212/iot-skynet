#include <ESP8266WiFi.h>
#include <../lib/arduino_secrets.h>
#include <SocketIoClient.h>

// Constants
SocketIoClient webSocket;
const char ssid[] = SECRET_SSID;
const char password[] = SECRET_PASS;
const int LED_MODULE = D5;

//Global Variables
int statusLight = 1;
String emitText = "";

//WebSocketHandlers
void event(const char * payload, size_t length) {
	// do stuff
  emitText = "{\"led\":";
  emitText += statusLight;
  emitText += "}";

  webSocket.emit("hw:dataInit", emitText.c_str());
}

void handleLight(const char * payload, size_t length) {
	// do stuff
  Serial.println(payload);
  if( strcmp(payload, "true") == 0){
    statusLight = 1;
  } else {
    statusLight = 0;
  }

  if( statusLight == 1){
    //Activar luz, pin on
    Serial.println("APAGADO");
    digitalWrite(LED_MODULE, HIGH);
  } else {
    //Desactivar Luz, pin off
    Serial.println("ENCENDIDO");
    digitalWrite(LED_MODULE, LOW);
  }
}

void getStatus(const char * payload, size_t length) {
	// do stuff
  emitText = "{\"led\":";
  emitText += statusLight;
  emitText += "}";

  webSocket.emit("hw:data", emitText.c_str());
}

void setup() {

  // Start Serial port
  Serial.begin(115200);

  pinMode(LED_MODULE, OUTPUT);

  // Connect to access point
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while ( WiFi.status() != WL_CONNECTED ) {
    delay(500);
    Serial.print(".");
  }

  // Print our IP address
  Serial.println("Connected!");
  Serial.print("My IP address: ");
  Serial.println(WiFi.localIP());

  // Start WebSocket server and assign callback
  webSocket.begin("10.106.1.135", 4042, "/socket.io/?transport=websocket");
  webSocket.on("connect", event);
  webSocket.on("hw:handleLight", handleLight);
  webSocket.on("hw:getStatus", getStatus);
}

void loop() {
  webSocket.loop();
}
