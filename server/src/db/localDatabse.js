import mongoose from 'mongoose'
const URI = 'mongodb://localhost/iotSkyNet'

export function tryConnect(){
  mongoose.connect(URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify:false })
    .then(db => console.log('DB is connected'))
    .catch(err => console.log('DB not connected'))
}
