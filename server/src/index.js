import express, {json} from 'express'
import SocketIO from 'socket.io'
import {tryConnect} from './db/localDatabse'
import cors from 'cors'
import {config} from 'dotenv'
import morgan from 'morgan'
import graphqlHTTP from 'express-graphql'
import schema from './graphql/schema'
import Hardware from './models/Hardware_data'


const NODE_ENV = process.env.NODE_ENV || 'development'

config({path: `../.env.${NODE_ENV}`})
tryConnect()
const app = express()

//Settings
app.set('port', process.env.PORT || 3030)

//middlewares
//morgan - peticiones navegador, cliente
app.use(morgan('dev'))
app.use(json())
app.use(cors())

//Routers
app.use('/graphql', graphqlHTTP({
  graphiql: true,
  schema
}))

//Starting the server
const server = app.listen(app.get('port'), () => console.log(`Server port ${app.get('port')}`))

//Websockets
const io = SocketIO(server)
io.on('connection', (socket) => {
  console.log('New Connection', socket.id)
  var intervaloPrueba

  socket.on('hw:dataInit', (data) => {
    console.log(data.led)

    if( data.led === 0){
      Hardware.findOneAndUpdate({hwId: 'K001'}, {status: false})
    } else {
      Hardware.findOneAndUpdate({hwId: 'K001'}, {status: true})
    }

    intervaloPrueba = setInterval( async () => {
      const dataHW = await Hardware.findOne({hwId: 'K001'})
      if( dataHW.status ){
        socket.emit("hw:handleLight", "true")
      } else {
        socket.emit("hw:handleLight", "false")
      }
    }, 5000)

  })

  socket.on('hw:data', (data) => {
    console.log(data)
    if(data.led === 0){
      socket.emit("hw:handleLight", "true")
    } else {
      socket.emit("hw:handleLight", "false")
    }
  })

  socket.on('disconnect', () => {
    console.log('User disconnected')
    clearInterval(intervaloPrueba)
  })
})
