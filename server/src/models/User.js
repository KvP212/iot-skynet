import {Schema, model} from 'mongoose'

const HardwaresSchema = new Schema({

  Hardware: {
    type: Schema.Types.ObjectId,
    ref: 'Hardware'
  },

  isAuth: {
    type: Boolean,
    default: false
  }

})

const userSchema = new Schema({
  firstName: {
    type: String,
    required: true
  },

  lastName: String,

  email: {
    type: String,
    required: true
  },

  pictureProfile: {
    type: String,
    default: "https://img.pngio.com/userpng-caltech-library-user-png-240_300.png"
  },

  typeUser: {
    type: String,
    default: "common"
  },

  Hardwares: [HardwaresSchema]

})

export default model('User', userSchema)
