import {Schema, model} from 'mongoose'

const authUsers = new Schema({

  User: {
    type: Schema.Types.ObjectId,
    ref: 'User'
  },

  isAuth: {
    type: Boolean,
    default: false
  }

})

const hardwareSchema = new Schema({
  hwId: {
    type: String,
    required: true
  },

  tag: {
    type: String,
    required: true
  },

  type: {
    type: String,
    required: true
  },

  location: {
    type: String,
    default: "lobby"
  },

  status: {
    type: Boolean,
    default: false
  },

  authUsers: [authUsers]
})

export default model('Hardware', hardwareSchema)
