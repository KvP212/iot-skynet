import {makeExecutableSchema} from 'graphql-tools'
import {resolvers} from './resolvers'


const typeDefs = `
  type Query {
    hello: String
    Users: [User]
    Hardwares: [Hardware]
    UserAuth(email: String): User
  }

  type Mutation {
    createUser(input: UserInput): String
    deleteUser(_id: ID): String
    updateUser(_id: ID, input: UserInputUpdate): String

    createHw(input: HwInput): String
    deleteHw(_id: ID): String

    addHwControl(_id: ID, input: UserInputUpdate): String
    deleteHwControl(_id: ID, idHW: ID): String
    updateHwControl(_id: ID, input: HwInputUpdate): String
  }

  type User{
    _id: ID
    firstName: String
    lastName: String
    email: String
    pictureProfile: String
    typeUser: String
    Hardwares: [hardwaresControl]
  }

  input UserInput {
    firstName: String!
    lastName: String
    email: String!
    pictureProfile: String
    typeUser: String
  }

  input UserInputUpdate {
    firstName: String
    lastName: String
    email: String
    pictureProfile: String
    typeUser: String
    Hardwares: hardwaresControlInput
  }

  type Hardware{
    _id: ID
    hwId: String
    tag: String
    type: String
    location: String
    status: Boolean
    authUsers: [AuthUser]
  }

  type AuthUser{
    _id: ID,
    User: User,
    isAuth: Boolean
  }

  input HwInput{
    hwId: String!
    tag: String!
    type: String!
    location: String
    authUsers: AuthUserInput
  }

  input AuthUserInput{
    User: ID,
    isAuth: Boolean
  }

  type hardwaresControl{
    _id: ID,
    Hardware: Hardware,
    isAuth: Boolean
  }

  input hardwaresControlInput{
    Hardware: ID
  }

  input HwInputUpdate{
    status: Boolean
  }
`

export default makeExecutableSchema({
  typeDefs,
  resolvers
})
