import User from '../models/User'
import Hardware from '../models/Hardware_data'

export const resolvers = {

  Query: {
    hello: () => 'Hello World with GraphQL',
    Users: async () => await User.find(),
    Hardwares: async () => await Hardware.find().populate('authUsers.User'),
    UserAuth: async (_, {email}) => await User.findOne({email}).populate('Hardwares.Hardware'),
  },

  Mutation: {
    createUser: async (_, {input}) => {
      const newUser = new User(input)
      await newUser.save()
      return "New User Save"
    },
    deleteUser: async (_, {_id}) => {
      await User.findByIdAndDelete(_id)
      return "User Deleted"
    },
    updateUser: async (_, {_id, input}) => {
      await User.findByIdAndUpdate(_id, input, { new: true})
      return "User Updated"
    },

    createHw: async (_, {input}) => {
      const newHw = new Hardware(input)
      await newHw.save()
      return "New HW Save"
    },
    deleteHw: async (_, {_id}) => {
      await Hardware.findByIdAndDelete(_id)
      return "Hw Deleted"
    },

    addHwControl: async(_, {_id, input}) => {
      await User.findByIdAndUpdate(_id, input, {new: true})
      return 'Add new HW to Control'
    },
    deleteHwControl: async(_, {_id, idHW}) => {
      const user = await User.findById(_id)
      const result = user.Hardwares.filter(hw => hw._id === idHW)

      user.Hardwares = result

      await user.save()

      return "HW Control Deleted"
    },
    updateHwControl: async(_, {_id, input}) => {
      await Hardware.findByIdAndUpdate(_id, input, {new: true})
      return 'HW Updated'
    }
  }
}
