export const tasks = [
  {
    _id: 0,
    title: 'Do homework',
    description: 'lorem ipsum',
    number: 100,
    isTrue: true
  },

  {
    _id: 1,
    title: 'Run',
    description: 'lorem ipsum',
    number: 100,
    isTrue: true
  },

  {
    _id: 2,
    title: 'Sleep',
    description: 'lorem ipsum',
    number: 100,
    isTrue: true
  }
]
