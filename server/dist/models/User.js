"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var HardwaresSchema = new _mongoose.Schema({
  Hardware: {
    type: _mongoose.Schema.Types.ObjectId,
    ref: 'Hardware'
  },
  isAuth: {
    type: Boolean,
    "default": false
  }
});
var userSchema = new _mongoose.Schema({
  firstName: {
    type: String,
    required: true
  },
  lastName: String,
  email: {
    type: String,
    required: true
  },
  pictureProfile: {
    type: String,
    "default": "https://img.pngio.com/userpng-caltech-library-user-png-240_300.png"
  },
  typeUser: {
    type: String,
    "default": "common"
  },
  Hardwares: [HardwaresSchema]
});

var _default = (0, _mongoose.model)('User', userSchema);

exports["default"] = _default;