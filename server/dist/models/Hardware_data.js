"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mongoose = require("mongoose");

var authUsers = new _mongoose.Schema({
  User: {
    type: _mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  isAuth: {
    type: Boolean,
    "default": false
  }
});
var hardwareSchema = new _mongoose.Schema({
  hwId: {
    type: String,
    required: true
  },
  tag: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  location: {
    type: String,
    "default": "lobby"
  },
  status: {
    type: Boolean,
    "default": false
  },
  authUsers: [authUsers]
});

var _default = (0, _mongoose.model)('Hardware', hardwareSchema);

exports["default"] = _default;