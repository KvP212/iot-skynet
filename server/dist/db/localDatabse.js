"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.tryConnect = tryConnect;

var _mongoose = _interopRequireDefault(require("mongoose"));

var URI = 'mongodb://localhost/iotSkyNet';

function tryConnect() {
  _mongoose["default"].connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  }).then(function (db) {
    return console.log('DB is connected');
  })["catch"](function (err) {
    return console.log('DB not connected');
  });
}