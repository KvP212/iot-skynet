"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _express = _interopRequireWildcard(require("express"));

var _socket = _interopRequireDefault(require("socket.io"));

var _localDatabse = require("./db/localDatabse");

var _cors = _interopRequireDefault(require("cors"));

var _dotenv = require("dotenv");

var _morgan = _interopRequireDefault(require("morgan"));

var _expressGraphql = _interopRequireDefault(require("express-graphql"));

var _schema = _interopRequireDefault(require("./graphql/schema"));

var _Hardware_data = _interopRequireDefault(require("./models/Hardware_data"));

var NODE_ENV = process.env.NODE_ENV || 'development';
(0, _dotenv.config)({
  path: "../.env.".concat(NODE_ENV)
});
(0, _localDatabse.tryConnect)();
var app = (0, _express["default"])(); //Settings

app.set('port', process.env.PORT || 3030); //middlewares
//morgan - peticiones navegador, cliente

app.use((0, _morgan["default"])('dev'));
app.use((0, _express.json)());
app.use((0, _cors["default"])()); //Routers

app.use('/graphql', (0, _expressGraphql["default"])({
  graphiql: true,
  schema: _schema["default"]
})); //Starting the server

var server = app.listen(app.get('port'), function () {
  return console.log("Server port ".concat(app.get('port')));
}); //Websockets

var io = (0, _socket["default"])(server);
io.on('connection', function (socket) {
  console.log('New Connection', socket.id);
  var intervaloPrueba;
  socket.on('hw:dataInit', function (data) {
    console.log(data.led);

    if (data.led === 0) {
      _Hardware_data["default"].findOneAndUpdate({
        hwId: 'K001'
      }, {
        status: false
      });
    } else {
      _Hardware_data["default"].findOneAndUpdate({
        hwId: 'K001'
      }, {
        status: true
      });
    }

    intervaloPrueba = setInterval(function _callee() {
      var dataHW;
      return _regenerator["default"].async(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _regenerator["default"].awrap(_Hardware_data["default"].findOne({
                hwId: 'K001'
              }));

            case 2:
              dataHW = _context.sent;

              if (dataHW.status) {
                socket.emit("hw:handleLight", "true");
              } else {
                socket.emit("hw:handleLight", "false");
              }

            case 4:
            case "end":
              return _context.stop();
          }
        }
      });
    }, 5000);
  });
  socket.on('hw:data', function (data) {
    console.log(data);

    if (data.led === 0) {
      socket.emit("hw:handleLight", "true");
    } else {
      socket.emit("hw:handleLight", "false");
    }
  });
  socket.on('disconnect', function () {
    console.log('User disconnected');
    clearInterval(intervaloPrueba);
  });
});