"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _graphqlTools = require("graphql-tools");

var _resolvers = require("./resolvers");

var typeDefs = "\n  type Query {\n    hello: String\n    Users: [User]\n    Hardwares: [Hardware]\n    UserAuth(email: String): User\n  }\n\n  type Mutation {\n    createUser(input: UserInput): String\n    deleteUser(_id: ID): String\n    updateUser(_id: ID, input: UserInputUpdate): String\n\n    createHw(input: HwInput): String\n    deleteHw(_id: ID): String\n\n    addHwControl(_id: ID, input: UserInputUpdate): String\n    deleteHwControl(_id: ID, idHW: ID): String\n    updateHwControl(_id: ID, input: HwInputUpdate): String\n  }\n\n  type User{\n    _id: ID\n    firstName: String\n    lastName: String\n    email: String\n    pictureProfile: String\n    typeUser: String\n    Hardwares: [hardwaresControl]\n  }\n\n  input UserInput {\n    firstName: String!\n    lastName: String\n    email: String!\n    pictureProfile: String\n    typeUser: String\n  }\n\n  input UserInputUpdate {\n    firstName: String\n    lastName: String\n    email: String\n    pictureProfile: String\n    typeUser: String\n    Hardwares: hardwaresControlInput\n  }\n\n  type Hardware{\n    _id: ID\n    hwId: String\n    tag: String\n    type: String\n    location: String\n    status: Boolean\n    authUsers: [AuthUser]\n  }\n\n  type AuthUser{\n    _id: ID,\n    User: User,\n    isAuth: Boolean\n  }\n\n  input HwInput{\n    hwId: String!\n    tag: String!\n    type: String!\n    location: String\n    authUsers: AuthUserInput\n  }\n\n  input AuthUserInput{\n    User: ID,\n    isAuth: Boolean\n  }\n\n  type hardwaresControl{\n    _id: ID,\n    Hardware: Hardware,\n    isAuth: Boolean\n  }\n\n  input hardwaresControlInput{\n    Hardware: ID\n  }\n\n  input HwInputUpdate{\n    status: Boolean\n  }\n";

var _default = (0, _graphqlTools.makeExecutableSchema)({
  typeDefs: typeDefs,
  resolvers: _resolvers.resolvers
});

exports["default"] = _default;