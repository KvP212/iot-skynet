"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resolvers = void 0;

var _regenerator = _interopRequireDefault(require("@babel/runtime/regenerator"));

var _User = _interopRequireDefault(require("../models/User"));

var _Hardware_data = _interopRequireDefault(require("../models/Hardware_data"));

var resolvers = {
  Query: {
    hello: function hello() {
      return 'Hello World with GraphQL';
    },
    Users: function Users() {
      return _regenerator["default"].async(function Users$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              _context.next = 2;
              return _regenerator["default"].awrap(_User["default"].find());

            case 2:
              return _context.abrupt("return", _context.sent);

            case 3:
            case "end":
              return _context.stop();
          }
        }
      });
    },
    Hardwares: function Hardwares() {
      return _regenerator["default"].async(function Hardwares$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return _regenerator["default"].awrap(_Hardware_data["default"].find().populate('authUsers.User'));

            case 2:
              return _context2.abrupt("return", _context2.sent);

            case 3:
            case "end":
              return _context2.stop();
          }
        }
      });
    },
    UserAuth: function UserAuth(_, _ref) {
      var email;
      return _regenerator["default"].async(function UserAuth$(_context3) {
        while (1) {
          switch (_context3.prev = _context3.next) {
            case 0:
              email = _ref.email;
              _context3.next = 3;
              return _regenerator["default"].awrap(_User["default"].findOne({
                email: email
              }).populate('Hardwares.Hardware'));

            case 3:
              return _context3.abrupt("return", _context3.sent);

            case 4:
            case "end":
              return _context3.stop();
          }
        }
      });
    }
  },
  Mutation: {
    createUser: function createUser(_, _ref2) {
      var input, newUser;
      return _regenerator["default"].async(function createUser$(_context4) {
        while (1) {
          switch (_context4.prev = _context4.next) {
            case 0:
              input = _ref2.input;
              newUser = new _User["default"](input);
              _context4.next = 4;
              return _regenerator["default"].awrap(newUser.save());

            case 4:
              return _context4.abrupt("return", "New User Save");

            case 5:
            case "end":
              return _context4.stop();
          }
        }
      });
    },
    deleteUser: function deleteUser(_, _ref3) {
      var _id;

      return _regenerator["default"].async(function deleteUser$(_context5) {
        while (1) {
          switch (_context5.prev = _context5.next) {
            case 0:
              _id = _ref3._id;
              _context5.next = 3;
              return _regenerator["default"].awrap(_User["default"].findByIdAndDelete(_id));

            case 3:
              return _context5.abrupt("return", "User Deleted");

            case 4:
            case "end":
              return _context5.stop();
          }
        }
      });
    },
    updateUser: function updateUser(_, _ref4) {
      var _id, input;

      return _regenerator["default"].async(function updateUser$(_context6) {
        while (1) {
          switch (_context6.prev = _context6.next) {
            case 0:
              _id = _ref4._id, input = _ref4.input;
              _context6.next = 3;
              return _regenerator["default"].awrap(_User["default"].findByIdAndUpdate(_id, input, {
                "new": true
              }));

            case 3:
              return _context6.abrupt("return", "User Updated");

            case 4:
            case "end":
              return _context6.stop();
          }
        }
      });
    },
    createHw: function createHw(_, _ref5) {
      var input, newHw;
      return _regenerator["default"].async(function createHw$(_context7) {
        while (1) {
          switch (_context7.prev = _context7.next) {
            case 0:
              input = _ref5.input;
              newHw = new _Hardware_data["default"](input);
              _context7.next = 4;
              return _regenerator["default"].awrap(newHw.save());

            case 4:
              return _context7.abrupt("return", "New HW Save");

            case 5:
            case "end":
              return _context7.stop();
          }
        }
      });
    },
    deleteHw: function deleteHw(_, _ref6) {
      var _id;

      return _regenerator["default"].async(function deleteHw$(_context8) {
        while (1) {
          switch (_context8.prev = _context8.next) {
            case 0:
              _id = _ref6._id;
              _context8.next = 3;
              return _regenerator["default"].awrap(_Hardware_data["default"].findByIdAndDelete(_id));

            case 3:
              return _context8.abrupt("return", "Hw Deleted");

            case 4:
            case "end":
              return _context8.stop();
          }
        }
      });
    },
    addHwControl: function addHwControl(_, _ref7) {
      var _id, input;

      return _regenerator["default"].async(function addHwControl$(_context9) {
        while (1) {
          switch (_context9.prev = _context9.next) {
            case 0:
              _id = _ref7._id, input = _ref7.input;
              _context9.next = 3;
              return _regenerator["default"].awrap(_User["default"].findByIdAndUpdate(_id, input, {
                "new": true
              }));

            case 3:
              return _context9.abrupt("return", 'Add new HW to Control');

            case 4:
            case "end":
              return _context9.stop();
          }
        }
      });
    },
    deleteHwControl: function deleteHwControl(_, _ref8) {
      var _id, idHW, user, result;

      return _regenerator["default"].async(function deleteHwControl$(_context10) {
        while (1) {
          switch (_context10.prev = _context10.next) {
            case 0:
              _id = _ref8._id, idHW = _ref8.idHW;
              _context10.next = 3;
              return _regenerator["default"].awrap(_User["default"].findById(_id));

            case 3:
              user = _context10.sent;
              result = user.Hardwares.filter(function (hw) {
                return hw._id === idHW;
              });
              user.Hardwares = result;
              _context10.next = 8;
              return _regenerator["default"].awrap(user.save());

            case 8:
              return _context10.abrupt("return", "HW Control Deleted");

            case 9:
            case "end":
              return _context10.stop();
          }
        }
      });
    },
    updateHwControl: function updateHwControl(_, _ref9) {
      var _id, input;

      return _regenerator["default"].async(function updateHwControl$(_context11) {
        while (1) {
          switch (_context11.prev = _context11.next) {
            case 0:
              _id = _ref9._id, input = _ref9.input;
              _context11.next = 3;
              return _regenerator["default"].awrap(_Hardware_data["default"].findByIdAndUpdate(_id, input, {
                "new": true
              }));

            case 3:
              return _context11.abrupt("return", 'HW Updated');

            case 4:
            case "end":
              return _context11.stop();
          }
        }
      });
    }
  }
};
exports.resolvers = resolvers;