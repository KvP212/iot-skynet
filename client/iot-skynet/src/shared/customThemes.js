import {DefaultTheme} from 'react-native-paper'
const customPrimary = '#ffeb3b'

export default customThemes = {
  Headline: {
    style: {
      color: customPrimary,
      lineHeight: 64,
      fontSize: 48
    }
  },
  Button:{
    style:{
      width: '50%',
      height: '30%'
    },
    mode: 'text',
    color: customPrimary
  },
  TextInput: {
    style:{
      width: '90%',
      margin: 10,
      backgroundColor: customPrimary,
      fontSize: 24
    },
    dense: true,
    mode: 'flat'
  },
  Text: {
    style:{
      color: '#fff'
    }
  },
  Cards: {
    style:{
      width: '90%',
      alignSelf: 'center',
      backgroundColor: customPrimary
    }
  }
}
