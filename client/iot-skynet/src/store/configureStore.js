import { createStore, combineReducers } from 'redux'
import countReducer from './reducers/countReducer'
import userReducer from './reducers/userReducer'
import boardsReducer from './reducers/boardsReducer'

const rootReducer = combineReducers({
  countReducer,
  userReducer,
  boardsReducer
})

const configureStore = () => {
  return createStore(rootReducer)
}

export default configureStore
