import {USER_INIT_LOGIN, HW_CONTROL_DATA} from '../constants'

const initialState = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
  pictureProfile: '',
  typeUser: '',
  Hardwares: []
}

const userReducer = (state = initialState, action) => {

  switch(action.type) {

    case USER_INIT_LOGIN:
      return {
      ...state,
      id: action.id,
      firstName: action.firstName,
      lastName: action.lastName,
      email: action.email,
      pictureProfile: action.pictureProfile,
      typeUser: action.typeUser,
      Hardwares: action.Hardwares
    }

    case HW_CONTROL_DATA:
      return{
        ...state,
        Hardwares: action.Hardwares
      }

    default:
      return state
  }
}

export default userReducer
