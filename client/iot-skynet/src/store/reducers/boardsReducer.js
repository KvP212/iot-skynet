import {HW_INIT_LOGIN} from '../constants'

const initialState = {
  hardwares:[]
}

const boardsReducer = (state = initialState, action) => {

  switch(action.type) {

    case HW_INIT_LOGIN:
      return {
      ...state,
      hardwares: action.hardwares
    }

    default:
      return state
  }
}

export default boardsReducer
