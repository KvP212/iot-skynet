import React, {useState, useEffect} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {HW_CONTROL_DATA} from '../../store/constants'
import { Platform, StatusBar, View, SafeAreaView, ScrollView, RefreshControl} from 'react-native'
import { Text, Headline, Card, Button, Title, Paragraph, Avatar } from 'react-native-paper'
import customThemes from '../../shared/customThemes'
import { GraphQLClient } from 'graphql-request'

const Control = () => {

  const dispatch = useDispatch()
  const hardwaresControl = useSelector( state => state.userReducer.Hardwares)
  const idUser = useSelector( state => state.userReducer.id)
  const emailUser = useSelector( state => state.userReducer.email)
  const [refreshing, setRefreshing] = useState(false)

  const onRefresh = () => {
    setRefreshing(true)

    setTimeout(() => {
      setRefreshing(false)
    }, 3000)
  }

  useEffect( () => {

  }, [refreshing])

  return (
    <View style={{flex: 1}}>

      <View style={{
          backgroundColor: '#000',
          height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
        }}>
        <StatusBar
        barStyle="light-content"/>
      </View>

      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#000'
        }}>
        <ScrollView
          refreshControl= {
            <RefreshControl
              refreshing={refreshing}
              onRefresh={onRefresh}
              tintColor='#fff' />
          }>

          <Headline style={customThemes.Headline.style}>{'Control'}</Headline>
          {
            hardwaresControl && hardwaresControl.map( hw => (
              <Card key={hw._id} style={customThemes.Cards.style}>

                <Card.Cover style={{height: 100}} source={{ uri: 'https://picsum.photos/700' }} />

                <Card.Content>
                  <Title>{hw.Hardware.tag}</Title>
                  <Paragraph>{ 'Location: ' + hw.Hardware.location }</Paragraph>
                  <Paragraph>{ 'Status: ' + hw.Hardware.status }</Paragraph>
                </Card.Content>

                <Card.Actions>
                  <Button
                    disabled={ hw.isAuth? false: true }
                    onPress={ async () => {
                      const endpoint = 'http://10.106.1.135:4042/graphql'
                      const client = new GraphQLClient(endpoint, {headers:{}})

                      const mutation = `mutation{
                        updateHwControl(_id: "${hw.Hardware._id}", input: {
                          status: ${hw.Hardware.status? false: true}
                        })
                      }`

                      try{
                        const data = await client.request(mutation)
                        if( data.updateHwControl === 'HW Updated'){

                          const query = `
                            query UserAuth($email: String) {
                              UserAuth(email: $email) {
                                Hardwares {
                                  _id,
                                  Hardware{
                                    _id,
                                    tag,
                                    location,
                                    status
                                  },
                                  isAuth
                                }
                              }
                            }
                          `
                          const variables = {"email": emailUser}

                          try {
                            const refreshData = await client.request(query, variables)

                            if(refreshData.UserAuth){
                              dispatch({
                                type:HW_CONTROL_DATA,
                                Hardwares: refreshData.UserAuth.Hardwares
                              })
                            }
                          } catch (error) {
                            console.log(error)
                          }

                        }

                      } catch (error){
                        console.log(error)
                      }

                    }}> Actions
                  </Button>

                  <Button onPress={ async () => {
                    const endpoint = 'http://10.106.1.135:4042/graphql'
                    const client = new GraphQLClient(endpoint, {headers:{}})

                    const mutation = `
                      mutation{
                        deleteHwControl(_id: "${idUser}", idHW: "${hw._id}")
                      }
                    `

                    try{
                      const data = await client.request(mutation)
                      if( data.deleteHwControl === 'HW Control Deleted'){

                        const query = `
                          query UserAuth($email: String) {
                            UserAuth(email: $email) {
                              Hardwares {
                                _id,
                                Hardware{
                                  _id,
                                  tag,
                                  location,
                                  status
                                },
                                isAuth
                              }
                            }
                          }
                        `
                        const variables = {"email": emailUser}

                        try {
                          const refreshData = await client.request(query, variables)

                          if(refreshData.UserAuth){
                            dispatch({
                              type:HW_CONTROL_DATA,
                              Hardwares: refreshData.UserAuth.Hardwares
                            })
                          }
                        } catch (error) {
                          console.log(error)
                        }

                      }

                    } catch (error){
                      console.log(error)
                    }

                  }}>Delete</Button>

                </Card.Actions>

              </Card>
            ))
          }

        </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default Control
