import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {HW_CONTROL_DATA} from '../../store/constants'
import { Platform, StatusBar, View, SafeAreaView, ScrollView, RefreshControl} from 'react-native'
import { Headline, Card, Button, Title, Paragraph, Avatar } from 'react-native-paper'
import customThemes from '../../shared/customThemes'
import { GraphQLClient } from 'graphql-request'

const Boards = () => {

  const dispatch = useDispatch()
  const hardwares = useSelector( state => state.boardsReducer.hardwares)
  const idUser = useSelector( state => state.userReducer.id)
  const emailUser = useSelector( state => state.userReducer.email)

  return (
    <View style={{flex: 1}}>

      <View style={{
          backgroundColor: '#000',
          height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
        }}>
        <StatusBar
        barStyle="light-content"/>
      </View>

      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#000'
        }}>
        <ScrollView>

          <Headline style={customThemes.Headline.style}>{'Boards'}</Headline>
          {
            hardwares && hardwares.map( hw => (
              <Card key={hw._id} style={customThemes.Cards.style}>

              <Card.Cover style={{height: 100}} source={{ uri: 'https://picsum.photos/700' }} />

              <Card.Content>
                <Title>{hw.tag}</Title>
                <Paragraph>{hw.location}</Paragraph>
              </Card.Content>

              <Card.Actions>
                <Button onPress={ async () => {
                  const endpoint = 'http://10.106.1.135:4042/graphql'
                  const client = new GraphQLClient(endpoint, {headers:{}})

                  const mutation = `
                    mutation{
                      addHwControl(_id: "${idUser}", input:{
                        Hardwares:{
                          Hardware: "${hw._id}"
                        }
                      })
                    }
                  `

                  try{
                    const data = await client.request(mutation)
                    if( data.addHwControl === 'Add new HW to Control'){

                      const query = `
                        query UserAuth($email: String) {
                          UserAuth(email: $email) {
                            Hardwares {
                              _id,
                              Hardware{
                                _id,
                                tag,
                                location
                              },
                              isAuth
                            }
                          }
                        }
                      `
                      const variables = {"email": emailUser}

                      try {
                        const refreshData = await client.request(query, variables)

                        if(refreshData.UserAuth){
                          dispatch({
                            type:HW_CONTROL_DATA,
                            Hardwares: refreshData.UserAuth.Hardwares
                          })
                        }
                      } catch (error) {
                        console.log(error)
                      }
                    }

                  } catch (error){
                    console.log(error)
                  }

                }}
                >Add</Button>
              </Card.Actions>

            </Card>
            ))
          }

        </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default Boards
