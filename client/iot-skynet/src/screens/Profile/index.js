import React from 'react'
import { useSelector } from 'react-redux'
import { Platform, StatusBar, View, SafeAreaView, ScrollView, RefreshControl} from 'react-native'
import { Button, Headline, Card, Avatar } from 'react-native-paper'
import customThemes from '../../shared/customThemes'

const Profile = ({navigation}) => {

  const firstName = useSelector( state => state.userReducer.firstName)
  const lastName = useSelector( state => state.userReducer.lastName)
  const email = useSelector( state => state.userReducer.email)
  const pictureProfile = useSelector( state => state.userReducer.pictureProfile)

  return (
    <View style={{flex: 1}}>

      <View style={{
          backgroundColor: '#000',
          height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
        }}>
        <StatusBar
        barStyle="light-content"/>
      </View>

      <SafeAreaView
        style={{
          flex: 1,
          backgroundColor: '#000'
        }}>
        <ScrollView>

          <Headline style={customThemes.Headline.style}>{'Profile'}</Headline>
          <Card style={customThemes.Cards.style}>
            <Card.Title
              title={firstName + ' ' + lastName}
              subtitle={email}
              left={(props) => <Avatar.Image {...props}
              source={{uri: pictureProfile}} />}
            />

            <Card.Actions>
              <Button
                mode= {customThemes.Button.mode}
                onPress = { () => navigation.navigate('Auth')}>
                  Log Out
              </Button>
            </Card.Actions>
          </Card>

        </ScrollView>
      </SafeAreaView>
    </View>
  )
}

export default Profile
