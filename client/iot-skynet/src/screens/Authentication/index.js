import React, {useState} from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {USER_INIT_LOGIN, HW_INIT_LOGIN} from '../../store/constants'
import { Platform, StatusBar, View, SafeAreaView } from 'react-native'
import { Button, TextInput } from 'react-native-paper'
import customThemes from '../../shared/customThemes'
import { GraphQLClient } from 'graphql-request'

const Authentication = ({navigation}) => {

  const [emailInput, setEmailInput] = useState('')
  const [passInput, setPassInput] = useState('')
  const dispatch = useDispatch()

  return  (
  <View style={{flex: 1}}>
    <View style={{
        backgroundColor: '#000',
        height: Platform.OS === 'ios' ? 20 : StatusBar.currentHeight
      }}>
      <StatusBar
      barStyle="light-content"/>
    </View>

    <SafeAreaView
      style={{
        flex: 1,
        flexDirection: 'column',
        backgroundColor: "#000",
        justifyContent: 'center',
        alignItems: 'center'
      }}>

      <TextInput
        label='Email'
        dense={customThemes.TextInput.dense}
        mode={customThemes.TextInput.mode}
        style={customThemes.TextInput.style}
        onChange={(e) => setEmailInput(e.nativeEvent.text.toLowerCase())}
      />

      <TextInput
        label='Password'
        dense={customThemes.TextInput.dense}
        mode={customThemes.TextInput.mode}
        style={customThemes.TextInput.style}
        secureTextEntry={ true }
        onChange={(e) => setPassInput(e.nativeEvent.text.toLowerCase())}
      />

      <Button
        mode= {customThemes.Button.mode}
        color= {customThemes.Button.color}
        style= {customThemes.Button.style}
        onPress = { async () => {
          const endpoint = 'http://10.106.1.135:4042/graphql'
          const client = new GraphQLClient(endpoint, {headers:{}})

          const query = `
            query UserAuth($email: String) {
              UserAuth(email: $email) {
                _id
                firstName
                lastName
                email
                pictureProfile
                typeUser,
                Hardwares{
                  _id,
                  Hardware{
                    _id,
                    tag,
                    location,
                    status
                  },
                  isAuth
                }
              },
              Hardwares {
                _id
                tag,
                location
              }
            }
          `
          const variables = {"email": emailInput}

          try {
            const data = await client.request(query, variables)
            if( data.UserAuth ){
              dispatch({
                type: USER_INIT_LOGIN,
                id: data.UserAuth._id,
                firstName: data.UserAuth.firstName,
                lastName: data.UserAuth.lastName,
                email: data.UserAuth.email,
                pictureProfile: data.UserAuth.pictureProfile,
                typeUser: data.UserAuth.typeUser,
                Hardwares: data.UserAuth.Hardwares
              })

              dispatch({
                type: HW_INIT_LOGIN,
                hardwares: data.Hardwares
              })
              console.log(JSON.stringify(data,undefined,2))
              navigation.navigate('MainTabs')
            }
          } catch (error) {
            console.log(error)
            // console.error(JSON.stringify(error, undefined, 2))
          }

        }}>
          Login
      </Button>

    </SafeAreaView>
  </View>
)}

Authentication.navigationOptions = {
  headerMode: "none"
}

export default Authentication
