import React from 'react';
import {Provider} from 'react-redux'
import configureStore from './src/store/configureStore'
import { DefaultTheme, Provider as PaperProvider, Text } from 'react-native-paper'
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import {createBottomTabNavigator} from 'react-navigation-tabs'
import { Ionicons } from '@expo/vector-icons'

import Control from './src/screens/Control'
import Boards from './src/screens/Boards'
import Profile from './src/screens/Profile'

import Authentication from './src/screens/Authentication'

const store = configureStore()

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#000'

  }
}

const DashTabNavigator = createBottomTabNavigator({
    Control: {
      screen : Control,
      navigationOptions: () => ({
        title: 'Control',
        tabBarIcon: ({focused, horizontal, tintColor}) => (
          <Ionicons
            name={'ios-switch'}
            size={ horizontal? 20 : 26 }
            color={tintColor}
          />
        ),
        tabBarLabel: ({focused, tintColor}) => (
          <Text style={{color: tintColor, fontSize: 12}}>{focused? 'Control': ''}</Text>
        )
      })
    },
    Boards: {
      screen: Boards,
      navigationOptions: () => ({
        title: 'Boards',
        tabBarIcon: ({focused, horizontal, tintColor}) => (
          <Ionicons
            name={'ios-cube'}
            size={ horizontal? 20 : 26 }
            color={tintColor}
          />
        ),
        tabBarLabel: ({focused, tintColor}) => (
          <Text style={{color: tintColor, fontSize: 12}}>{focused? 'Boards': ''}</Text>
        )
      })
    },
    Profile: {
      screen: Profile,
      navigationOptions: () => ({
        title: 'Profile',
        tabBarIcon: ({focused, horizontal, tintColor}) => (
          <Ionicons
            name={'ios-contact'}
            size={ horizontal? 20 : 26 }
            color={tintColor}
          />
        ),
        tabBarLabel: ({focused, tintColor}) => (
        <Text style={{color: tintColor, fontSize: 12}}>{focused? 'Profile': ''}</Text>
        )

      })
    }
  },{
    tabBarOptions:{
      style:{
        backgroundColor: 'rgba(0,0,0,0.9)',
        position: 'absolute',
        borderTopWidth: 0.2
      },
      activeTintColor: '#ffeb3b'
    }
  }

)

const AppSwitchNavigator = createSwitchNavigator({
    Auth: Authentication,
    MainTabs: DashTabNavigator
  }
)

const AppContainer = createAppContainer(AppSwitchNavigator)

export default function App() {
  return (
    <Provider store={store}>
      <PaperProvider theme={theme}>

        <AppContainer/>

      </PaperProvider>
    </Provider>
  )
}
